package guru.springframework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/* Create by springframeworkguru
 * Clone from https://github.com/springframeworkguru/spring-boot-redis-example
 */
@SpringBootApplication
public class SpringBootRedisApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootRedisApplication.class, args);
	}
}
