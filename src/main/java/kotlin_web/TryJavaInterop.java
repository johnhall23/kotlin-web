package kotlin_web;

public class TryJavaInterop {
    public String greeting;
    
    public TryJavaInterop (String greeting) {
        this.greeting = greeting;
        System.out.println(greeting);
    }
    
    public void setGreeting (String greeting) {
        this.greeting = greeting;
    }
    
    public String tryGreeting () {
        return greeting;
    }
}
