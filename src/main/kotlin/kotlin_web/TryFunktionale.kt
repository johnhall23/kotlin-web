package kotlin_web

import org.funktionale.collections.*
import org.funktionale.composition.compose
import org.funktionale.composition.forwardCompose

data class TryFunktionale (val numbers: List<Number>) {

    fun getDestructuredHeadAndBody (): List<Any> {
        val (head, body) = numbers.destructured()
        print("Get head (first) and body of list using funktionale destructured() => ")

        return listOf(head, body)
    }

    fun getFunctionFromComposition(composeType: String = "compose"): (Int) -> Int {
        val add5 = { i: Int -> i + 5 }
        val times2 = { i: Int -> i * 2 }

        return if (composeType.equals("forwardCompose")) (add5 forwardCompose times2)
            else (add5 compose times2)
    }
}