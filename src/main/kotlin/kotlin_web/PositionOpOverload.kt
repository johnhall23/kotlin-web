package kotlin_web

data class PositionOpOverload(var x: Int, var y: Int) {
    operator fun plus(other: PositionOpOverload): PositionOpOverload {
        return PositionOpOverload(x + other.x, y + other.y)
    }
}