package kotlin_web

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class KotlinWebApplication

fun main(args: Array<String>) {
    SpringApplication.run(KotlinWebApplication::class.java, *args)
}
