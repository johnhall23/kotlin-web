package kotlin_web

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(value = "/", produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
class KotlinWebController() {

    @RequestMapping(value = "", method = arrayOf(RequestMethod.GET))
    fun get(/*@PathVariable key: String*/): Any {
        //return object {
        //    val greeting = "Hello, Kotlin, from the web"
        //}
        return TryJavaInterop("Hello, Kotlin, from the web")
    }

}