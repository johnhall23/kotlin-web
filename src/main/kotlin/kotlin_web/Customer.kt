package kotlin_web

data class Customer (val name: String, var orders: Int) {}
