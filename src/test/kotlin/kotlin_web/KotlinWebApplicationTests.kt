package kotlin_web

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import kotlin.reflect.full.memberProperties
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.redis.core.ValueOperations





@RunWith(SpringRunner::class)
@SpringBootTest
class KotlinWebApplicationTests {

    @Autowired
    private lateinit var template: StringRedisTemplate

	@Test
	fun main(): Unit {
        val greeting = "Hello " // Types can be inferred from the declared value

        testJavaInterop(greeting);

        val aVal: Int // Types must be included in declaration if value is not provided. Int is short for kotlin.Int
        aVal = 1


        /* All type conversions must be explicit.
         * Each kotlin type has conversion methods.
         */
        var aLong: Long = aVal.toLong()

        val aString: String
        aString = if (aLong > 0) aLong.toString() else aVal.toString()
        /* Above is the same as: */
        //if (aLong > 0) {
        //    aString = aLong.toString()
        //} else {
        //    aString = aVal.toString()
        //};


        println()
        println("Do/While Loop:")
        println()

        aLong = -9

        while (-10 < aLong.toInt() && aLong.toInt() < 10) { //do {

            when { // (aLong.toInt()) {
            //1,2,3,4,5,6,7,8,9 -> println(aLong.toString() +": "+ greeting)
                (aLong.toInt() <= 0) -> {
                    println("0: "+ greeting +"Kotlin")
                    aLong = incrementNumber(aLong, 10).toLong()
                }
                (aLong.toInt() in 1..9) -> {
                    println(aLong.toString() +": "+ greeting +"Kotlin")
                    aLong = incrementNumber(aLong).toLong() // Same as incrementNumber(aLong, 1).toLong()
                }
                else -> {
                    println("$aString: "+ greeting +"Kotlin")
                }
            }

        } //while (-10 < aLong.toInt() && aLong.toInt() < 10)


        /* Above is the same as: */
        println()
        println("For Loop with Label:")
        println()

        forLoop@for (anIterator in -9..9) {
            when {
                (anIterator <= 0) -> {
                    if (anIterator == 0) println("0: "+ greeting +"Kotlin")
                    else continue@forLoop // Need a label to break *outer* for loop
                }
                (anIterator in 1..9) -> {
                    println("$anIterator: "+ greeting +"Kotlin")
                }
                else -> {
                    println("$aString: "+ greeting +"Kotlin")
                }
            }
        }

        /* Operator Overloading used by data class */
        val p = testOpOverload();

        println()
        println("testOpOveload returns ${p.toString()}");

        /* Destructuring */
        val (xPos, yPos) = p;
        println("Destructured PositionOpOverload xPos=$xPos, yPos=$yPos");

        /* Reflection */
        println("Class (simple) name = ${p.javaClass.kotlin.simpleName}")
        for (propertyName in p.javaClass.kotlin.memberProperties) {
            print("Property (simple) name = ${propertyName.name}\n");
            println("$propertyName");
        }

        var aLambda: (Customer) -> String // Create a var to hold a lambda literal
        aLambda = { a: Customer ->
            val customerString = a.name +" has "+ a.orders +" orders"
            println(customerString);
            customerString
        }

        /* Functional Programming */
        testFunctionalCustomers(
            listOf(
                Customer("John", 23),
                Customer("Hannah", 22),
                Customer("Mike", 1001),
                Customer("Rich", 1)
            ),
            aLambda // Passing a lambda variable
        );

        /* Use methods from the funktionale library */
        var numSeq = generateSequence(1){ it + 1 }.take(5); // Generate a sequence of numbers

        testFunktionaleList(numSeq.toList()); // Pass result of converting sequence into list

        testFunktionaleComposition(numSeq.last().toInt()); // Pass last sequence value to composed function
	}

    @Test
    //@Throws(Exception::class)
    fun testRedis() {
        val ops = this.template.opsForValue()
        val key = "spring.boot.redis.test"
        if (!this.template.hasKey(key)) {
            ops.set(key, "foo")
        }
        println("Found key " + key + ", value=" + ops.get(key))
    }

}


//fun incrementLong (aLong: Long, interval: Int): Long {
//    val result = aLong + interval
//    return result
//}

//fun incrementLong (aLong: Long, interval: Int): Long = aLong + interval;

fun <T: Number> incrementNumber (aNumber: T, interval: Int = 1): Int = interval.plus(aNumber.toInt())

fun testJavaInterop(g: String): Unit {  // Return type kotlin.Unit is Kotlin's void type
    val tryJava = TryJavaInterop(g +"Java");

    tryJava.greeting = "Yo Java"; // Public properties of Java objects must have getters; otherwise considered constants

    println("${tryJava.tryGreeting()} from Kotlin");
}

fun testOpOverload (): PositionOpOverload {
    val p1 = PositionOpOverload(100, 200)
    val p2 = PositionOpOverload(1000, 2000)
    val p3 = p1 + p2

    return p3
}

fun testFunctionalCustomers (customers: List<Customer>, customerMap: (Customer) -> String): List<String> { // Expecting a lambda parameter
    var mostOrders: Customer? = (customers.maxBy{ it.orders });

    //println(customers.maxBy{ a -> a.orders })
    println("${mostOrders?.name} has the most orders") // In lambda, a single default argument is assumed to be "it"

    return customers.map( customerMap );
}

fun testFunktionaleList (listOfNumbers: List<Number> = listOf(1, 2)) { // List of Numbers parameter has default values
    val (head, body) = TryFunktionale(listOfNumbers).getDestructuredHeadAndBody();
    println("Head: $head, Body: $body");
}

fun testFunktionaleComposition (aNumber: Int) {
    val tryFunktionale: TryFunktionale = TryFunktionale(listOf(aNumber))

    val multiplyBy2ThenAdd5 = tryFunktionale.getFunctionFromComposition(); // Default arg is "compose"
    println("Result from composed function => $aNumber * 2 + 5 = ${multiplyBy2ThenAdd5(aNumber)}")

    val add5ThenMultiplyBy2 = tryFunktionale.getFunctionFromComposition("forwardCompose");
    println("Result from composed function => $aNumber + 5 * 2 = ${add5ThenMultiplyBy2(aNumber)}")
}


